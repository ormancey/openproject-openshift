#!/bin/bash

# Environment needed
# SECRET_KEY_BASE=secret

#EMAIL_DELIVERY_METHOD=smtp
#SMTP_ADDRESS=smtp.sendgrid.net
#SMTP_PORT=587
#SMTP_DOMAIN=my.domain.com
#SMTP_AUTHENTICATION=login
#SMTP_ENABLE_STARTTLS_AUTO=true
#SMTP_USER_NAME="apikey"
#SMTP_PASSWORD=

# DATABASE_URL=postgres://openproject:openproject@database:5432/openproject
# DATABASE_URL=mysql2://user:pass@host:port/dbname

indent() {
	sed -u 's/^/       /'
}

migrate() {
	pushd /usr/src/app
#	/etc/init.d/memcached start
    /usr/bin/memcached &
	rake db:migrate db:seed
#	/etc/init.d/memcached stop
    kill `pidof memcached`
    # not permitted to change grp from root to app
	#chown app:app db/schema.rb 
	popd
}


echo "-----> Using an external database, migrating.."
migrate | indent

echo "-----> Launching supervisord..."
exec /usr/bin/supervisord


trap : TERM INT; sleep infinity & wait