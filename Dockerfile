#FROM centos:centos7
#FROM openproject/community:7-base
FROM openproject/community:7

MAINTAINER "Emmanuel Ormancey <emmanuel.ormancey@cern.ch>"

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY passwd /etc/passwd

RUN mkdir -p /usr/src/app /usr/local/bundle /var/log/supervisor &&\
    chown -R -f app /usr/src/app /home/app &&\
    chmod -R a+r /usr/src/app /usr/local/bundle &&\
    chmod -R 777 /var/log/supervisor /var/run /etc/apache2/sites-enabled /var/lock &&\
    mkdir -p /var/lock/apache2 && chown -f www-data /var/lock/apache2

COPY run.sh /run.sh
RUN chmod a+x /run.sh
CMD ["/run.sh"] 

EXPOSE 8080
